#!/usr/bin/make -f
SHELL=/bin/bash

SRC_DIR=src
BIN_DIR=bin

CC=cc
CFLAGS=-Wall --pedantic
LDFLAGS=

.PHONY:	clean all

everything:	clean all

clean:
	find $(BIN_DIR) -mindepth 1 -maxdepth 1 -type f -executable -print -delete

all:
	ls -1 $(SRC_DIR)/*.c | \
	xargs -r -t basename -s .c | \
	xargs -r -t $(MAKE)

%: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(BIN_DIR)/$@ $<

